<?php
include 'entities/User.php';

use entities\User;
use entities\UserSocial;

/**
 * @author Исаков Владислав
 */
class Sanitaizer {
	/** @var string */
	public string $jsonStr;

	public array $result = [];

	/**
	 * Список классов, которые может обработать сантитайзер
	 */
	const ALLOWED_ENTITIES = [
		User::class,
		UserSocial::class
	];

	/**
	 * @param string $jsonStr
	 *
	 * @author Исаков Владислав
	 *
	 * Sanitaizer constructor.
	 */
	public function __construct(string $jsonStr) {
		$this->jsonStr = $jsonStr;
	}

	/**
	 * Конвертирование json-обьектов источника в поддерживаемые обьекты
	 *
	 * @return null
	 *
	 * @author Исаков Владислав
	 */
	public function convertToEntities() {
		$sourceObjects = json_decode($this->jsonStr);
		if (is_array($sourceObjects)) {
			foreach ($sourceObjects as $object) {
				$this->result[] = static::validateObject($object);
			}

			return;
		}

		$this->result[] = static::validateObject($sourceObjects);
	}

	/**
	 * @param $sourceObject
	 *
	 * @return \entities\BaseEntity|void
	 *
	 * @author Исаков Владислав
	 */
	public static function validateObject($sourceObject) {
		$foundedEntityClass = null;

		foreach (static::ALLOWED_ENTITIES as $entityClass) {
			$entityClassProperties = array_keys(get_class_vars($entityClass));
			$sourceObjectProperties = array_keys(get_object_vars($sourceObject));

			sort($entityClassProperties);
			sort($sourceObjectProperties);

			if ($entityClassProperties === $sourceObjectProperties) {
				$foundedEntityClass = $entityClass;
			}
		}

		if (null === $foundedEntityClass) {
			print_r('Object is not allowed: ' . json_encode($sourceObject));

			die;
		}

		//рукурсивно идем по вложенным массивам обьектов
		foreach ($sourceObject as $sourceSubObjects) {
			if (is_array($sourceSubObjects)) {
				foreach ($sourceSubObjects as $sourceSubObject) {
					static::validateObject($sourceSubObject);
				}
			}
		}

		/** @var \entities\BaseEntity $entityObject */
		$entityObject = new $foundedEntityClass;
		$entityObject->convert($sourceObject);

		if ($entityObject->hasErrors()) {
			print_r($entityObject->getErrors());

			die;
		}

		return $entityObject;
	}
}