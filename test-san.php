<?php
include 'Sanitaizer.php';

set_error_handler(function ($err_severity, $err_msg, $err_file, $err_line) {
	throw new ErrorException ($err_msg, 0, $err_severity, $err_file, $err_line);
});

//$sanitizer = new Sanitaizer('{"name": "vlad", "id": "1", "phone": "8 (950) 288-56-23"}');
//$sanitizer = new Sanitaizer('[{"name": "vlad", "id": "1", "phone": "8 (950) 288-56-23"},{"name": "test2", "id": "2", "phone": "8 (952) 288-56-23"}]');
//$sanitizer = new Sanitaizer('[{"social":[{"id": 1,"title":"vkontakte"}, {"id": 2,"title":"odnoklassniki"}], "name": "vlad", "id": "1", "phone": "8 (950) 288-56-23"},{"name": "test2", "id": "2", "phone": "8 (952) 288-56-23"}]');
$sanitizer = new Sanitaizer('[{"name": "vlad", "id": "1h", "phone": "8 (950) 288-56-23", "social":[{"id": "1","title":"vkontakte"}, {"id": "2","title":"odnoklassniki"}]}]');

$sanitizer->convertToEntities();

print_r($sanitizer->result);