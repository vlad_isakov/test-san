<?php

namespace entities;

include 'entities/traits/ConvertTrait.php';
include 'entities/interfaces/IEntity.php';
include 'BaseEntity.php';
include 'UserSocial.php';

use entities\interfaces\IEntity;
use entities\traits\ConvertTrait;

/**
 * Class User
 *
 *
 * @author  Исаков Владислав
 */
class User extends BaseEntity implements IEntity {
	use ConvertTrait;

	/** @var int */
	public int $id;

	/** @var string */
	public string $name;

	/** @var string */
	public string $phone;

	/** @var \entities\UserSocial[] */
	public array $social;

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function specificConvert() {
		if (false === static::isPhoneValid($this->phone, true)) {
			$this->addError('Wrong phone number');
		}

		// оставляем только цифры
		$this->phone = preg_replace('/[^0-9]+/', '', $this->phone);
		// если нет ведущего кода страны (определяем по количеству цифр), то проставим
		if (strlen($this->phone) === 10) {
			$this->phone = '7' . $this->phone;
		}
		// меняем код страны с 8 на 7
		if (substr($this->phone, 0, 1) === '8') {
			$this->phone = '7' . substr($this->phone, 1, strlen($this->phone) - 1);
		}
	}

	/**
	 * @param string $phone
	 * @param bool   $mobileOnly
	 *
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	public static function isPhoneValid(string $phone, $mobileOnly = false) {
		$phoneStripped = preg_replace('/[^0-9\.\-\s\(\)\+\_\,\*]+/usi', '', $phone);// Удаляем всё, что не используется в номере телефона

		// -- Если в результате удаления оказалось, что в строке ещё что-то было
		if ($phoneStripped != $phone) {
			return false;
		}


		$phone = preg_replace('/[^0-9]+/', '', $phone);
		$phone = trim($phone);

		// -- Если значение содержит неверные данные
		if (1 !== preg_match('/^(8|7|)(\d)(\d{9})$/usi', $phone, $matches)) {
			return false;
		}
		// -- -- -- --

		// -- Если разрешено вводить только номера мобильных телефонов (в номерах мобильных телефонов вторая цифра всегда 9).
		if (true === $mobileOnly) {
			if (11 !== strlen($phone)) {
				return false;
			}
			if (9 != $matches[2] && 7 != $matches[2]) {
				return false;
			}
		}

		return true;
	}
}