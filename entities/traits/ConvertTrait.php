<?php

namespace entities\traits;

/**
 * ОБщий валидатор-конвертер для встроенных типов на основе рефлексии
 */
trait ConvertTrait {

	/**
	 * @param $sourceObject
	 *
	 * @throws \ReflectionException
	 *
	 * @author Исаков Владислав
	 */
	public function convert($sourceObject) {
		$refClass = new \ReflectionClass($this);
		$refParams = $refClass->getProperties();

		foreach ($refParams as $refParam) {
			//Не Builtin типы тут пропускаем
			if (false === $refParam->getType()->isBuiltin()) {
				continue;
			}

			$paramName = $refParam->name;
			try {
				$this->$paramName = $sourceObject->$paramName;
			}
			catch (\Exception $exception) {
				$this->addError('Wrong parameter [' . $paramName . '] type: ' . $sourceObject->$paramName);

				return;
			}
		}

		//Если есть что-то специфичное, такое как телефон, то проверим/сконвертируем
		$this->specificConvert();
	}
}