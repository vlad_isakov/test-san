<?php

namespace entities\interfaces;

/**
 * Interface IEntity
 *
 * @author  Исаков Владислав
 */
interface IEntity {

	/**
	 * Специфичные для конкретной сущности валидации и конвертации
	 *
	 * @author Исаков Владислав
	 */
	public function specificConvert();
}