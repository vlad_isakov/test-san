<?php

namespace entities;

use entities\interfaces\IEntity;
use entities\traits\ConvertTrait;

/**
 * Class UserSocial
 *
 * @package entities
 *
 * @author  Исаков Владислав
 */
class UserSocial extends BaseEntity implements IEntity {
	use ConvertTrait;
	/** @var int */
	public int $id;

	/** @var string */
	public string $title;

	public function specificConvert() {
	}
}