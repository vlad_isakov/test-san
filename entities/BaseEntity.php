<?php

namespace entities;

/**
 * Базовый класс для приемников
 *
 * @package entities
 *
 * @author  Исаков Владислав
 */
class BaseEntity {

	private array $_errors = [];

	public function addError(string $msg) {
		$this->_errors[] = $msg;
	}

	/**
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	public function hasErrors(): bool {
		return count($this->_errors) > 0;
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function getErrors(): string {
		return json_encode($this->_errors);
	}
}